#version 150

in  vec3 inPosition;
//in  vec3 inColor;
in  vec3 inNormal;

out vec3 exNormal; // Phong

void main(void)
{
	exNormal = inNormal; // Phong
	
	gl_Position = vec4(inPosition, 1.0);
}
