// Run code:
// gcc voronoi.c -lm -o voronoi && ./voronoi
//

#include<stdio.h>
#include <stdlib.h>
#include <math.h>
#include "voronoi.h"
#include<float.h>

Point* debugPoints;
int numDebugPoints;

Line* debugLines;
int numDebugLines;

Line* sepLines;
Line** allSepLines;
Point* crossPoints;
PointArray* allCrossPoints;
Point* seeds;
Point* clearPoints;
int seedCount;
int allLineCount;

Line** lineSets;
PointArray* cellSets;

Point* originalPolyCorners;
int numOriginalPolyCorners;

typedef enum {false = 0, true = 1 } bool;

struct Point createPoint(float x, float y, float z) {
    Point p;
    p.x = x;
    p.y = y;
    p.z = z;
    return p;
}
struct PointArray createPointArray(Point* p, int size) {
    PointArray pointArray;
    pointArray.array = p;
    pointArray.arraySize = size;

    return pointArray;
}

struct Line createLine(float k, float m, float x) {
    Line l;
    l.k = k;
    l.m = m;
    l.x = x;
    return l;
}
Line createLineWithPoints(Point p1, Point p2) {
    float k,m,x;
    Line line;

    if (fabs(p1.x - p2.x) > FLT_EPSILON) { // not equal to
        k = (p1.y - p2.y) / (p1.x - p2.x);
        m = p1.y - (k * p1.x);
        x = FLT_MAX;
    }else{
        k = FLT_MAX;
        m = FLT_MAX;
        x = p1.x;
    }
    line.x = x;
    line.k = k;
    line.m = m;
    return line;
}
bool isLineVertical(Line l) {
    if (l.k == FLT_MAX || l.m == FLT_MAX)
        return true;  
    return false;
}

bool isLinesParallel(Line l1, Line l2) {
    if ((l1.k == FLT_MAX) && (l2.k == FLT_MAX)) {
        return true;  
    }
    return false;
}

Point getLineCrossPoint(Line l1, Line l2) {
    float x,y;
    if (isLinesParallel(l1, l2)) {
        x = FLT_MAX;
        y = FLT_MAX;
    } else if(isLineVertical(l1) && !isLineVertical(l2)){
        x = l1.x;
        y = l2.k * x + l2.m;
    } else if(!isLineVertical(l1) && isLineVertical(l2)){
        x = l2.x;
        y = l1.k * x + l1.m;
    } else {
        x = ((l2.m - l1.m)/(l1.k - l2.k));
        y = l1.k * x + l1.m;
    }

    return createPoint(x, y, 0.0f);
}

int crossCount;
int allCrossCount;
int clearCount;
int i;
int j;
int k;
PointArray* getAllLineCrossPoints(Line** lineSets, int numSeeds) {
    Point* pointArray;
    numDebugPoints = 0;

    int numLines = numSeeds - 1 + numOriginalPolyCorners; // lines per cell, including lines for the original polygon.
    cellSets = malloc (sizeof (PointArray) * (numSeeds)); //numSeeds = amount of cells

    for (k = 0; k < numSeeds; k++) {
        PointArray crossPointsArray;
        allCrossCount = 0;
        pointArray = malloc (sizeof (Point) * (pow(numLines,2))); // Wors case allocation. See dynamic arrays for better alternative.
        for (i = 0; i < numLines; i++) {
            for (j = i; j < numLines; j++) {
                if (i != j && lineSets[k][i].k != lineSets[k][j].k) {
                    pointArray[allCrossCount] = getLineCrossPoint(lineSets[k][i], lineSets[k][j]);
                    allCrossCount++;
                }
            }
        }
        crossPointsArray.array = pointArray;
        crossPointsArray.arraySize = allCrossCount;
        cellSets[k] = crossPointsArray;
        if (k == 0) {
            debugPoints = pointArray;
            numDebugPoints = allCrossCount;
        }
    }
    return cellSets;
}

float randomFloat(float top){
    return ((float)rand()/(float)(RAND_MAX)) * top;
}

Point* generatePoints(Point ref, int num_points, float max_x, float max_y){
    Point *pointArray = malloc (sizeof (Point) * num_points);
    float weight = 0.0;
    float concentration = 0.25; // [.2, .5] 
    for(i = 0; i < num_points; ++i){
        pointArray[i].x = randomFloat(max_x);
        pointArray[i].y = randomFloat(max_y);
        pointArray[i].z = 0;

        if (distance(ref, pointArray[i]) > concentration)  // alt.2
            weight = (concentration / distance(ref, pointArray[i]));

        //weight = -distance(ref, pointArray[i]) + 1; // Linear distribution

        pointArray[i].x -= (pointArray[i].x - ref.x) * weight;
        pointArray[i].y -= (pointArray[i].y - ref.y) * weight;
        pointArray[i].z -= (pointArray[i].z - ref.z) * weight;
    }
    return pointArray;
}

Line* concatinateLines(Line* a, Line* b, int aSize, int bSize) {
    int i,j;
    Line* temp = malloc (sizeof(Line) * (aSize + bSize));
    for (i = 0; i < aSize; i++) {
        temp[i] = a[i];
    }
    int tempIndex = 0;
    for (j = i; j < i + bSize; j++) {
        temp[j] = b[tempIndex];
        tempIndex++;
    }
    return temp;
}

Line* generateOriginalPolyLines() {
    int i,j;
    Line* temp = malloc (sizeof(Line) * numOriginalPolyCorners);
    for (i = 0; i < numOriginalPolyCorners; i++) {
        if (i != numOriginalPolyCorners) {
            temp[i] = createLineWithPoints(originalPolyCorners[i], originalPolyCorners[i+1]); 
        }else {
            temp[i] = createLineWithPoints(originalPolyCorners[i], originalPolyCorners[0]);
        }
    }
    return temp;
}

Line createSeparatingLine(Point p1, Point p2)
{
    float k, m, x;
    if(p1.y!=p2.y) {
        k = (p2.x - p1.x)/(p1.y - p2.y);
        m = 0.5 * (p1.y + p2.y) - k * 0.5 * (p1.x + p2.x);
        x = FLT_MAX;
    }
    else {
        printf("infinite k, HELP%\n");
        k=FLT_MAX;
        m=FLT_MAX;
        x = fabs(p1.x - p2.x);
    }

    return createLine(k,m,x);
}

Line** generateAllSeparatingLines(Point* seedArray, int numSeeds) { //numSepLines: (numSeeds -1)*(numSeeds)
    int i,j;
    Line *lineArray;
    lineSets = malloc (sizeof (Line*) * (numSeeds));
    for (i = 0; i < numSeeds; i++) {
        allLineCount = 0;
        lineArray = malloc (sizeof (Line) * (numSeeds - 1));
        for (j = 0; j < numSeeds; j++) {
            if (i != j) {
                lineArray[allLineCount] = createSeparatingLine(seedArray[i], seedArray[j]);
                allLineCount++;
            }
        }
        lineSets[i] = concatinateLines(lineArray, generateOriginalPolyLines(), allLineCount, numOriginalPolyCorners);
    } 
    return lineSets;
}

float distance(Point p1, Point p2) {
    return sqrt( pow((p1.x - p2.x), 2) + pow((p1.y - p2.y), 2));
}

PointArray* clearAllCrossPoints(int cells, PointArray* allCrossPoints, int allCrossCount, Line** allSepLines, int numSepLines) 
{
    PointArray* allClearPoints = malloc (sizeof(PointArray) * cells);
    Point refPoint;
    Line beam;
    Point* tempCrossPoints;
    Point* clearPoints;
    int i,j,k,l,tempCrossCount,addClearPoint;

    Point tempPoint;

    //Debugline:
    printf("allCrossCount: %d\n", allCrossCount);
    debugLines = malloc (sizeof (Line) * allCrossCount);

    for (l = 0; l < cells; l++) { // cells
        refPoint = seeds[l];
        clearPoints = malloc (sizeof (Point) * allCrossCount);
        clearCount = 0;

        for (i = 0; i < allCrossCount; i++) { //allCrossCount
            tempCrossPoints = NULL;
            tempCrossPoints = malloc (sizeof (Point) * numSepLines);
            tempCrossCount = 0;
            addClearPoint = 1;
            beam = createLineWithPoints(refPoint, allCrossPoints[l].array[i]);
            //Debug beamlines
            if (l == 0) {
                debugLines[i] = beam;
                numDebugLines = i;
            }

            for (j = 0; j < numSepLines; j++) { // New crosspoints created by the beam
                tempPoint = getLineCrossPoint(beam, allSepLines[l][j]);
                if (tempPoint.x != FLT_MAX && tempPoint.y != FLT_MAX) {
                    tempCrossPoints[tempCrossCount] = tempPoint;
                    tempCrossCount++;
                }
            }

            for (k = 0; k < tempCrossCount; k++) { // Check: If crosspoints are on the same side of the referencePoint and a beamed crosspoint is closer.
                if (distance(tempCrossPoints[k], refPoint) + 0.01f < distance(allCrossPoints[l].array[i], refPoint)) {// need arb. small float because equality check is acting up. 
                    if ((tempCrossPoints[k].x < refPoint.x && allCrossPoints[l].array[i].x < refPoint.x) ||
                            (tempCrossPoints[k].x > refPoint.x && allCrossPoints[l].array[i].x > refPoint.x)) {
                        addClearPoint = 0;
                    }
                }
            }

            if (addClearPoint == 1) {
                clearPoints[clearCount] = allCrossPoints[l].array[i];
                clearCount++;
            }
        }
        allClearPoints[l].array = clearPoints;
        allClearPoints[l].arraySize = clearCount;
    }
    return allClearPoints;
}

float maxX;
float minX;
float maxY;
float minY;
Point* getPolygonMinMax(Point* polyCorners, int numPolyCorners) {
    maxX = 0.0f;
    minX = 0.0f;
    maxY = 0.0f;
    minY = 0.0f;
    Point* minMax = malloc (sizeof (Point) * 2);
    for (i = 0; i < numPolyCorners; i++) {
        if (polyCorners[i].x > maxX) {
            maxX = polyCorners[i].x;
        }
        if (polyCorners[i].x < minX) {
            minX = polyCorners[i].x;
        }
        if (polyCorners[i].y > maxY) {
            maxY = polyCorners[i].y;
        }
        if (polyCorners[i].y < minY) {
            minY = polyCorners[i].y;
        }
    }
    minMax[0] = createPoint(minX, minY, 0.0f);
    minMax[1] = createPoint(maxX, maxY, 0.0f);
    return minMax;
}

Point* concatinatePointArrays(Point* a, Point* b, int aSize, int bSize) {
    Point* temp = malloc (sizeof(Point) * (aSize + bSize));
    for (i = 0; i < aSize; i++) {
        temp[i] = a[i]; 
    }
    int tempIndex = 0;
    for (j = i; j < i + bSize; j++) {
        temp[j] = b[tempIndex];
        tempIndex++;
    }
    return temp;
}

void printOutPoint(Point p) {
    printf("\nPoint: \n");
    printf("x: %f, y: %f\n", p.x, p.y);
}
void printOutPoints(Point* p, int numPoints) {
    printf("Points Incomming: \n");
    for(i = 0; i < numPoints ; ++i) {
        printf("x: %f, y: %f\n", p[i].x, p[i].y);
    }
    printf("\n");
}
void printOutLines(Line* l, int numLines) {
    printf("\nLiness Incomming: \n");
    for(i = 0; i < numLines ; ++i) {
        printf("k: %f, m: %f", l[i].k, l[i].m);
        if (l[i].x != FLT_MAX) {
            printf(", x: %f", l[i].x);
        }
        printf("\n");
    }
}

float xMax;
float xMin;
float yMax;
float yMin;
PointArray* generateFullVoronoi(Point ref, int numSeeds, Point* polyCorners, int numPolyCorners){

    srand(time(NULL));
    PointArray* cellPoints;
    originalPolyCorners = polyCorners;
    numOriginalPolyCorners = numPolyCorners;

    seedCount = numSeeds;
    int numSepLines = seedCount - 1 + numPolyCorners;

    Point* minMax = getPolygonMinMax(polyCorners, numPolyCorners);

    if (numPolyCorners != 0) {    
        xMax = minMax[1].x;
        xMin = minMax[0].x;

        yMax = minMax[1].y;
        yMin = minMax[0].y;
    }else {
        xMax = 1.0f;
        xMin = 0.0f;

        yMax = 1.0f;
        yMin = 0.0f;
    }

    seeds = generatePoints(ref, numSeeds, xMax, yMax); // x,y -MIN IS 0 .. Change if needed
    printOutPoints(seeds, seedCount);


    allSepLines = generateAllSeparatingLines(seeds, seedCount);
    sepLines = allSepLines[0];
    printOutLines(allSepLines[0], numSepLines); //tests the first set.

    allCrossPoints = getAllLineCrossPoints(allSepLines, seedCount); 
    printOutPoints(allCrossPoints[0].array, allCrossPoints[0].arraySize);

    cellPoints = clearAllCrossPoints(numSeeds, allCrossPoints, allCrossCount, allSepLines, numSepLines); 

    int cellI;
    for(cellI = 0; cellI < numSeeds; cellI++) {
        printOutPoints(cellPoints[cellI].array, cellPoints[cellI].arraySize);
    }


    return cellPoints;
}

int getClearCount(void) {
    return clearCount;
}

int getSepLinesCount(void) {
    return seedCount - 1;
}

Point* getSeeds(void){
    return seeds;
}

Line* getSepLines(void){
    return sepLines;
}

int getNumDebugPoints(void) {
    return numDebugPoints;
}

Point* getDebugPoints(void){
    return debugPoints;
}

int getNumDebugLines(void) {
    return numDebugLines + 1;
}

Line* getDebugLines(void){
    return debugLines; //Need one more increment
}
/*int main( int arc, char **argv) {
// For rand
srand(time(NULL));

// numSeeds, polyCorners*, numPolycorners : #, Null, 0 if no object
generateFullVoronoi(4, NULL, 0);
}*/
