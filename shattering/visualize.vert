/*
 * random comment here
 * makes syntax highlight appaer
 * colors like springs sprouts
 */

#version 150

in  vec3  in_Position;
in  vec3  in_Normal;
//in  vec2  in_TexCoord;




uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

out vec3 out_Normal; 
out vec3 ex_position;
//out vec2 frag_texcoord;

void main(void)
{

	//shade = (mat3(viewMatrix)*in_Normal).z; // Fake shading
	out_Normal = normalize(mat3(viewMatrix) * in_Normal);

	//frag_texcoord = in_TexCoord;

	ex_position = normalize(mat3(viewMatrix) * in_Position);

	gl_Position = projectionMatrix*viewMatrix*vec4(in_Position, 1.0);
}

