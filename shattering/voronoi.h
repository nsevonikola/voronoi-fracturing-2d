typedef struct Line {
    float k, m, x;
} Line;

typedef struct Point {
    float x, y, z;
} Point;

typedef struct PointArray {
    Point* array;
    int arraySize;

} PointArray;

struct Point createPoint(float x, float y, float z);
struct Line createLine(float k, float m , float x);
struct PointArray createPointArray(Point* p, int size);

Line createLineWithPoints(Point p1, Point p2);
Point* generatePoints(Point ref, int num_points, float max_x, float max_y);
Point getLineCrossPoint(Line l1, Line l2);
Line createSeparatingLine(Point p1, Point p2);
int getClearCount(void);
Point* getSeeds(void);
int getSepLinesCount(void);
Line* getSepLines(void);
Point* concatinatePointArrays(Point* a, Point* b, int aSize, int bSize);

Point* generatePartialVoronoi(int numSeeds, Point* polyCorners, int numPolyCorners);
PointArray* generateFullVoronoi(Point ref, int numSeeds, Point* polyCorners, int numPolyCorners);

float distance(Point p1, Point p2);
float randomFloat(float top);

int getNumDebugPoints(void);
Point* getDebugPoints(void);

int getNumDebugLines(void);
Line* getDebugLines(void);

void printOutPoint(Point p);
void printOutPoints(Point* p, int numPoints);
