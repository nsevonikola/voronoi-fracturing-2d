//gcc shattering2.c voronoi.c ../common/*.c ../common/Linux/MicroGlut.c -lGL -o shattering -I../common -I../common/Linux && ./shattering
//MAC:
//gcc shattering2.c voronoi.c ../common/*.c ../common/mac/MicroGlut.m -o shattering -framework OpenGL -framework Cocoa -I../common/mac -I../common && ./shattering


// Converted to MicroGlut and VectorUtils3 2013.
// MicroGLut currently exists for Linux and Mac OS X, and a beta for Windows.
// You will also need GLEW for Windows. You may sometimes need to work around
// differences, e.g. additions in MicroGlut that don't exist in FreeGlut.

// 2015: 

// Linux: gcc lab0.c ../common/*.c ../common/Linux/MicroGlut.c -lGL -o lab0 -I../common -I../common/Linux

// Mac: gcc lab0.c ../common/*.c ../common/Mac/MicroGlut.m -o lab0 -framework OpenGL -framework Cocoa -I../common/Mac -I../common

#ifdef __APPLE__
	#include <OpenGL/gl3.h>
	#include "MicroGlut.h"
	//uses framework Cocoa
#else
	#include <GL/gl.h>
	#include "MicroGlut.h"
#endif
#include "GL_utilities.h"
#include "VectorUtils3.h"
#include "loadobj.h"
//#include "zpr.h"
#include "LoadTGA.h"
#include <stdlib.h>
#include <stdio.h>
#include "voronoi.h"
#include <math.h>

#define DEPTH 0.001


float phi= 2.7f; //Ändras till 0.0 för face view NIKOLA!!!
float r = 1.5f;

// Model-to-world matrix
mat4 modelToWorldMatrix;
mat4 identity = {{ 1.0, 0.0, 0.0, 0.0,
                   0.0, 1.0, 0.0, 0.0,
                   0.0, 0.0, 1.0, 0.0,
                   0.0, 0.0, 0.0, 1.0}};
GLfloat vertices[] = {0.0f, 0.0f, -0.01f,
		      1.0f, 0.0f, -0.01f,
		      1.0f, 1.0f, -0.01f,
		      0.0f, 1.0f, -0.01f
};/*
GLfloat vertices[] = {0.0f, 0.0f, -0.01f,
		      1.0f, 0.001f, -0.01f,
		      0.999f, 1.0005f, -0.01f,
		      0.0005f, 1.0f, -0.01f
};
/*GLfloat vertices[] = {0.0f, 0.0f, -0.01f,
		      1.0f, 0.3f, -0.01f,
		      0.8f, 0.7f, -0.01f,
		      0.3f, 1.0f, -0.01f,
};*/
int numPolyCorners = 4;
/*
GLfloat vertices[] = {0.0f, 0.6f, 0.0f,
		      0.0f, 0.4f, 0.0f, 
		      0.6f, 1.0f, 0.0f,
		      0.4f, 1.0f, 0.0f,
};*/

// World-to-view matrix. Usually set by lookAt() or similar.
mat4 viewMatrix;
// Projection matrix, set by a call to perspective().
mat4 projectionMatrix;

struct 

// Globals
// * Model(s)
Model *point_representation;
//Model *line_representation;
//Model *square;
//Model *shape;

int numCellShapes;

typedef struct Chard Chard;
struct Chard {
   Model *model;
   Point mid;
};

Chard *cellShapes;


GLfloat linevertex[12];
// * Reference(s) to shader program(s)
GLuint program;
GLuint visualizer;
// * Texture(s)
GLuint texture;
vec3* Position;
vec3* Velocity;
vec3* Rotation;
vec3* angularVelocity;
Point reference;

Point example_point;
Line example_line;
int numSeeds;

int* draw_bool;



Model* create_model(GLfloat* corners, int num_corners, float depth)
{
	int num_face_triangles = num_corners-2;
	GLuint *indexArray = malloc(sizeof(GLuint) * 3 * (num_face_triangles * 2 + 2 * num_corners) );

	GLfloat *normalArray = malloc(sizeof(GLfloat) * 2 * 3 * num_corners);
	GLfloat *vertexArray = malloc(sizeof(GLfloat) * 2 * 3 * num_corners);

	int i;

	for(i=0; i<num_corners; i++)
	{
		vertexArray[3 * i] =  corners[3 * i];
		vertexArray[3 * i + 1] = corners[3 * i + 1];
		vertexArray[3 * i + 2] = depth / 2;

		vertexArray[3 * num_corners + 3 * i] = corners[3 * i];
		vertexArray[3 * num_corners + 3 * i + 1] = corners[3 * i + 1];
		vertexArray[3 * num_corners + 3 * i + 2] = -depth / 2;

		printf(" vertexNumber %d has value ( %f , %f ) \n", i, vertexArray[3 * num_corners + 3 * i], vertexArray[3 * num_corners + 3 * i + 1]);
		
		normalArray[3*i] = 0.0f;
		normalArray[3*i + 1] = 0.0f;
		normalArray[3*i + 2] = 1.0f;

		normalArray[3 * num_corners + 3*i] =  0.0f;
		normalArray[3 * num_corners + 3*i + 1] = 0.0f;
		normalArray[3 * num_corners + 3*i + 2] = -1.0f;

	}

	for(i=0; i<num_face_triangles; i++)
	{
		indexArray[3 * i]= 0;
		indexArray[3 * i + 1]= i+1;
		indexArray[3 * i + 2]= i+2;

		indexArray[3 * num_face_triangles + 3 * i]= num_corners;
		indexArray[3 * num_face_triangles + 3 * i + 1]= num_corners + i + 2;
		indexArray[3 * num_face_triangles + 3 * i + 2]= num_corners + i + 1;
	}

	for(i = num_face_triangles; i < num_face_triangles + num_corners -1; i++)
	{	
		indexArray[6 * num_face_triangles + 6 * i]= i;
		indexArray[6 * num_face_triangles + 6 * i + 1]= i + num_corners;
		indexArray[6 * num_face_triangles + 6 * i + 2]= i + num_corners + 1;

		indexArray[6 * num_face_triangles + 6 * i + 3]= i;
		indexArray[6 * num_face_triangles + 6 * i + 4]= i + num_corners + 1;
		indexArray[6 * num_face_triangles + 6 * i + 5]= i + 1;
	}
/*
	indexArray[6 * num_face_triangles + 6 * (num_corners -1)]= (num_corners -1);
	indexArray[6 * num_face_triangles + 6 * (num_corners -1) + 1]= (num_corners -1) + num_corners;
	indexArray[6 * num_face_triangles + 6 * (num_corners -1) + 2]= num_corners;

	indexArray[6 * num_face_triangles + 6 * (num_corners -1) + 3]= (num_corners -1);
	indexArray[6 * num_face_triangles + 6 * (num_corners -1) + 4]= num_corners;
	indexArray[6 * num_face_triangles + 6 * (num_corners -1) + 5]= 0;
*/

	
	Model* model = LoadDataToModel(
			vertexArray,
			NULL, //normalArray, //Normal array
			NULL,//Texcoord
			NULL,
			indexArray,
			num_corners*2,
			6 * num_face_triangles + 6 * (num_corners-1));

	return model;
}



mat4 visualize_point(Point p)
{
	mat4 M;
	M = Mult(T(p.x, p.y, DEPTH/2 ),Rx(1.5703));
	M = Mult(M, S(0.004f, 0.004f, 0.004f));
	return M;
}

mat4 visualize_shard(int i)
{

	float dt =  1.0 / 60.0;
	mat4 m, rotation;
	Velocity[i].y += 0;//-0.01;
	Position[i].x += Velocity[i].x * dt; 
	Position[i].y += Velocity[i].y * dt; 
	Position[i].z += Velocity[i].z * dt; 
	Rotation[i].x += angularVelocity[i].x * dt;
	Rotation[i].y += angularVelocity[i].y * dt;
	Rotation[i].z += angularVelocity[i].z * dt;
	rotation = Mult ( Mult (Rx(Rotation[i].x), Ry(Rotation[i].y)), Rz(Rotation[i].z));
	
	rotation = Mult(Mult( T( cellShapes[i].mid.x,  cellShapes[i].mid.y, 0),rotation) , T( -cellShapes[i].mid.x,  -cellShapes[i].mid.y, 0));

	m = Mult(T(Position[i].x, Position[i].y, Position[i].z),rotation);
	return m;
}

void visualize_line(Line l)
{
	float dw = 0.01;
	int i = 0;
	if(l.m >= 0.0 && l.m < 1.0)
	{
		linevertex[6*i] = 0.0;
		linevertex[6*i+1] = l.m+dw;
		linevertex[6*i+2] = DEPTH/2 + dw;
		linevertex[6*i+3] = 0.0;
		linevertex[6*i+4] = l.m-dw;
		linevertex[6*i+5] = DEPTH/2 + dw;
		i++;

	}
	if((-l.m/l.k) > 0.0 && (-l.m/l.k) <= 1.0)
	{
		linevertex[6*i] = -l.m/l.k-dw;
		linevertex[6*i+1] = 0.0;
		linevertex[6*i+2] = DEPTH/2 + dw;
		linevertex[6*i+3] = -l.m/l.k+dw;
		linevertex[6*i+4] = 0.0;
		linevertex[6*i+5] = DEPTH/2 + dw;
		i++;
	}
	if((l.m+l.k) > 0.0 && (l.m+l.k) <= 1.0)
	{
		linevertex[6*i] = 1.0;
		linevertex[6*i+1] = l.m+l.k-dw;
		linevertex[6*i+2] = DEPTH/2 + dw;
		linevertex[6*i+3] = 1.0;
		linevertex[6*i+4] = l.m+l.k+dw;
		linevertex[6*i+5] = DEPTH/2 + dw;
		i++;
	}
	if(((1.0-l.m)/l.k) >= 0.0 && ((1.0-l.m)/l.k) < 1.0)
	{
		linevertex[6*i] = (1.0-l.m)/l.k+dw;
		linevertex[6*i+1] = 1.0;
		linevertex[6*i+2] = DEPTH/2 + dw;
		linevertex[6*i+3] = (1.0-l.m)/l.k-dw;
		linevertex[6*i+4] = 1.0;
		linevertex[6*i+5] = DEPTH/2 + dw;
		i++;

	}
		
}

Point* vertToPoint(GLfloat vertices[], int corners) {
	int i;
	Point *pointArray = malloc (sizeof (Point) * corners);
	for(i = 0; i < corners; i++) {
		pointArray[i] = createPoint(
                vertices[3 * i + 0], 
                vertices[3 * i + 1], 
                vertices[3 * i + 2] );
	}
	return pointArray;
}


int less(Point a, Point b, float cx, float cy)
{
    if (a.x - cx >= 0 && b.x - cx < 0)
        return 1;
    if (a.x - cx < 0 && b.x - cx >= 0)
        return 0;
    if (a.x - cx == 0 && b.x - cx == 0) {
        if (a.y - cy >= 0 || b.y - cy >= 0)
            return a.y > b.y;
        return b.y > a.y;
    }

    // compute the cross product of vectors (center -> a) x (center -> b)
    float det = (a.x - cx) * (b.y - cy) - (b.x - cx) * (a.y - cy);
    if (det < 0)
        return 1;
    if (det > 0)
        return 0;

    // points a and b are on the same line from the center
    // check which point is closer to the center
    int d1 = (a.x - cx) * (a.x - cx) + (a.y - cy) * (a.y - cy);
    int d2 = (b.x - cx) * (b.x - cx) + (b.y - cy) * (b.y - cy);
    return d1 > d2;
}

Point find_center(Point* cell, int numPoints)
{
	float cx = 0;
	float cy = 0;
	int i;
	Point mid_point;
	
	for(i = 0; i < numPoints; i++)
	{
		cx += cell[i].x;
		cy += cell[i].y;
	}
	mid_point.x = cx / (float) numPoints;
	mid_point.y = cy / (float) numPoints;

	return mid_point;
}
void sort_points(Point* cell, int numPoints)
{
	float cx = 0;
	float cy = 0;
	int i;
	Point aux;
	int found = 1; 

	for(i = 0; i < numPoints; i++)
	{
		cx += cell[i].x;
		cy += cell[i].y;
	}
	cx = cx / (float) numPoints;
	cy = cy / (float) numPoints;
	
	while(found)
	{
		found = 0;
		for(i=0; i<numPoints-1; i++)
		{
			if(less(cell[i], cell[i+1], cx, cy))
			{
				aux = cell[i];
				cell[i] = cell[i+1];
				cell[i+1] = aux;
				found = 1;
			}
		}
	}
	printf("Ordered points: \n");
	printOutPoints(cell, numPoints);

	return;
}

float distance_between_points( Point a, Point b)
{
	return sqrt( (a.x - b.x)*(a.x - b.x) +	
			       (a.y - b.y)*(a.y - b.y));
}

Model* points_to_shape(Point* pointArray, int num_corner)
{
	int i;
	sort_points(pointArray, num_corner);
	GLfloat* corners = malloc(sizeof(GLfloat) * 3 * num_corner);
	for(i=0; i<num_corner; i++)
	{
		corners[3*i] = pointArray[i].x; 
		corners[3*i + 1] = pointArray[i].y; 
		corners[3*i + 2] = 0;
	} 
	return create_model(corners, num_corner, DEPTH);
}
PointArray* pa;
void init(void)
{
	dumpInfo();

	// GL inits
	glClearColor(0.2,0.2,0.5,0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	printError("GL inits");

	projectionMatrix = perspective(90, 1.0, 0.1, 1000);


	// Load and compile shader
	visualizer = loadShaders("visualize.vert", "visualize.frag");
	program = loadShaders("lab0.vert", "lab0.frag");
	printError("init shader");
	
	// Upload geometry to the GPU:
	point_representation = LoadModelPlus("estrellica.obj");
	
	printError("load models");

	// Load textures
	LoadTGATextureSimple("textures/maskros512.tga",&texture);
	printError("load textures");

	/*
	example_point.x = 0.0f;
	example_point.y = 1.0f;
	example_point.z = 0.0f;

	example_line.k = -1;
	example_line.m = 1;
*/ 
	//visualize_line(example_line);
	//line_representation = create_model(linevertex, 4, DEPTH +0.01) .;


	//visualize created points
	numSeeds = 10;
    	numCellShapes = numSeeds;
	Point* polyCorners = vertToPoint(vertices, numPolyCorners);
    	reference = createPoint(0.5, 0.5 , 0);
	printf("PolyCorner: \n");
	printOutPoints(polyCorners, numPolyCorners);
	//cell = generatePartialVoronoi(numSeeds, NULL, 0);
	pa = generateFullVoronoi(reference, numSeeds, polyCorners, numPolyCorners);

	//seeds = getSeeds();
	//sepLines = getSepLines();

	
	//square = create_model(vertices, 4, DEPTH);

    int i;
    cellShapes = malloc ((sizeof (Model*)+sizeof ( Point)) * (numSeeds));
    draw_bool = malloc(sizeof(int) * numCellShapes);
    Position = malloc(sizeof(vec3) * numCellShapes);
    Velocity = malloc(sizeof(vec3) * numCellShapes);
    Rotation = malloc(sizeof(vec3) * numCellShapes);
    angularVelocity = malloc(sizeof(vec3) * numCellShapes);

    for (i = 0; i < numSeeds; i++) {
	float inv_distance = distance_between_points(cellShapes[i].mid,reference) - sqrt(2);//not realy inverted
        cellShapes[i].model = points_to_shape(pa[i].array, pa[i].arraySize); 
	cellShapes[i].mid = find_center(pa[i].array, pa[i].arraySize);
	draw_bool[i] = 1;
	Position[i].x = 0;
	Position[i].y = 0;
	Position[i].z = 0;

	Velocity[i].x = 0.1 * (cellShapes[i].mid.x - reference.x);
	Velocity[i].y = 0.1 * (cellShapes[i].mid.y - reference.y);
	Velocity[i].z = 0.1 * (inv_distance);

	Rotation[i].x = 0;
	Rotation[i].y = 0;
	Rotation[i].z = 0;

	angularVelocity[i].x = randomFloat(-inv_distance*inv_distance);
	angularVelocity[i].y = randomFloat(-inv_distance*inv_distance);
	angularVelocity[i].z = 0;
    }
    //angularVelocity[0].x = 1;
}



//void generate 
void display(void)
{
	int i;
	int j;
	printError("pre display");

	// clear the screen
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	GLfloat t = (GLfloat)glutGet(GLUT_ELAPSED_TIME);


	if (keyIsDown('a'))
	{	
		phi -= 0.01;
	}
	else if (keyIsDown('d'))
	{
		phi += 0.01;
	}	

	viewMatrix = lookAt(r*sin(phi) + 0.5, 0.5, r*cos(phi), 0.5,0.5,0, 0,1,0);

	//activate the visualization shader - FOR STARS ANR LINES
	GLint color = 8;
	glUseProgram(program);
	glUniformMatrix4fv(glGetUniformLocation(program, "projectionMatrix"), 1, GL_TRUE, projectionMatrix.m);
	


	/*-----BACKGROUND------*/

    	//Square Object
	//glUniform1i( glGetUniformLocation(program, "color"), color);
	//DrawModel(square, program, "in_Position", "in_Normal", NULL);

	/*-----BACKGROUND-END------*/



	/*-----CELL SHAPES------*/
    	for (i = 0; i < numCellShapes; i++) { // numCellShapes
            if(draw_bool[i])
            {
                modelToWorldMatrix = visualize_shard(i);
                mat4 m = Mult(viewMatrix, modelToWorldMatrix);
                    color = i % 8;
                glUniformMatrix4fv(glGetUniformLocation(program, "viewMatrix"), 1, GL_TRUE, m.m);
                    glUniform1i( glGetUniformLocation(program, "color"), color);
                    DrawModel(cellShapes[i].model, program, "in_Position", "in_Normal", NULL);
            }
    	}

	/*-----CELL SHAPES -END------*/



	//activate the visualization shader - FOR STARS ANR LINES
	glUseProgram(visualizer);
	glUniformMatrix4fv(glGetUniformLocation(visualizer, "projectionMatrix"), 1, GL_TRUE, projectionMatrix.m);

	/*-----STARS------*/

	 // Visualise Seeds
	/*for(i=0; i < numSeeds; i++) {
		modelToWorldMatrix = visualize_point(getSeeds()[i]);
		mat4 m = Mult(viewMatrix, modelToWorldMatrix);
		//draw the point
		glUniformMatrix4fv(glGetUniformLocation(visualizer, "viewMatrix"), 1, GL_TRUE, m.m);
		DrawModel(point_representation, visualizer, "in_Position", "in_Normal", NULL);
	}*/

	
    	//Cross points of each cell set
	/*for(i = 0; i < getNumDebugPoints(); i++) { // cell = seeds TODO: DONT FORGET TO CHANGE THE 1
            modelToWorldMatrix = visualize_point(getDebugPoints()[i]);
            mat4 m = Mult(viewMatrix, modelToWorldMatrix);
            //draw the point
            glUniformMatrix4fv(glGetUniformLocation(visualizer, "viewMatrix"), 1, GL_TRUE, m.m);
            DrawModel(point_representation, visualizer, "in_Position", "in_Normal", NULL);
	}*/


    	//Cross points of each cell set
	/*for(i = 0; i < 1; i++) { // cell = seeds TODO: DONT FORGET TO CHANGE THE 1, OR?
       	 	for (j = 0; j < pa[i].arraySize; j++) {
         	   modelToWorldMatrix = visualize_point(pa[i].array[j]);
         	   mat4 m = Mult(viewMatrix, modelToWorldMatrix);
         	   //draw the point
         	   glUniformMatrix4fv(glGetUniformLocation(visualizer, "viewMatrix"), 1, GL_TRUE, m.m);
         	   DrawModel(point_representation, visualizer, "in_Position", "in_Normal", NULL);
        	}
	}*/

	/*-----STARS-END------*/


	/*-----	LINES------*/

	 /* //Original shape Lines + voronoi divider
    	for(i=0; i < numSeeds - 1 + numPolyCorners; i++){ // + numPolyCorners
		visualize_line(getSepLines()[i]);
		line_representation = create_model(linevertex, 4, DEPTH + 0.01);
		glUniformMatrix4fv(glGetUniformLocation(visualizer, "viewMatrix"), 1, GL_TRUE, viewMatrix.m);
		DrawModel(line_representation, visualizer, "in_Position", "in_Normal", NULL);
	}*/


	// Visualizes BEAMS
   	/* for (i = 0; i < getNumDebugLines(); i++) {
		visualize_line(getDebugLines()[i]);
		line_representation = create_model(linevertex, 4 , DEPTH + 0.01);
		glUniformMatrix4fv(glGetUniformLocation(visualizer, "viewMatrix"), 1, GL_TRUE, viewMatrix.m);
		DrawModel(line_representation, visualizer, "in_Position", "in_Normal", NULL);
   	 }*/

	/*-----LINES-END------*/


	



	//glUniform1i(glGetUniformLocation(visualizer,"tex1"),0);//the last argument has to be the same as the texture-unit that is to be used

	//glActiveTexture(GL_TEXTURE0);//which texture-unit is active

	//glBindTexture(GL_TEXTURE_2D, texture);//load the texture to active texture-unit
	
	printError("display");
	
	glutSwapBuffers();
}

int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA|GLUT_DEPTH|GLUT_DOUBLE);
	initKeymapManager();
	glutInitContextVersion(3, 2);
	glutCreateWindow ("Voronoi Shattering");
	init ();
	glutDisplayFunc(display); 
	glutRepeatingTimer(20);
	glutMainLoop();
	exit(0);
}

