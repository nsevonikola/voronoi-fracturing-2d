/*
 * random comment here
 * makes syntax highlight appaer
 * colors like springs sprouts
 */

#version 150

//in vec3 out_Normal;
in vec3 ex_position;
//in vec2 frag_texcoord;

//uniform sampler2D tex1
uniform int color; 

out vec4 out_Color;

void main(void)
{
	//vec3 light = vec3(0, 0, 1.0);
	
	//float diffuse = dot(light,out_Normal);
	//vec3 reflection = reflect(light, out_Normal);
	//float specular = dot(reflection, -ex_position);	

	//float shade = diffuse * 0.3 + 0.7 * pow(specular, 15);
	float shade = 1.0;
	//out_Color=texture(tex1,frag_texcoord);
	if(color==0)
	{
		out_Color=vec4(0,0,0,1.0);
	}else if(color==1)
	{
		out_Color=vec4(0,0,shade,1.0);
	}else if(color==2)
	{
		out_Color=vec4(0,shade,0,1.0);
	}else if(color==3)
	{
		out_Color=vec4(0,shade,shade,1.0);
	}else if(color==4)
	{
		out_Color=vec4(shade,0,0,1.0);
	}else if(color==5)
	{
		out_Color=vec4(shade,0,shade,1.0);
	}else if(color==6)
	{
		out_Color=vec4(shade,shade,0,1.0);
	}else 
	{
		out_Color=vec4(shade,shade,shade,1.0);
	}
}

